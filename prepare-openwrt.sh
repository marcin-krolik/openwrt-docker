#!/bin/sh
set -e
#cp "openwrt-configs/openwrt-defconfig-${ARCH}" openwrt/.config
ARCH=${ARCH:-$(uname -m)}
#git clone --depth=1 https://github.com/mikma/lxd-openwrt
if [ ! -d openwrt ]; then
	git clone https://github.com/openwrt/openwrt
else
	(cd openwrt && git pull)
fi
if [ ! -d dl ]; then
	mkdir dl
else
	echo "Download folder present:"
	du -h dl
fi
#rm -rf openwrt/package/system/procd/patches
#cp -r lxd-openwrt/patches/procd-master openwrt/package/system/procd/patches
cd openwrt
make defconfig
./scripts/feeds update
./scripts/feeds install luci
cp "../openwrt-configs/container-defconfig-${ARCH}" .config
echo "CONFIG_DOWNLOAD_FOLDER=\"$(realpath ../dl)\"" >> .config
make defconfig
make -j$(nproc) prepare
